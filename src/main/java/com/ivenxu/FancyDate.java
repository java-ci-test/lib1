package com.ivenxu;

import java.time.LocalDate;

public class FancyDate {
    public LocalDate now() {
        return LocalDate.now();
    }

    /**
     * Echo hello.
     * 
     * @return the salutation with class name
     */
    public String hello() {
        return String.format("Hello from %s", this.getClass().getName());
    }
}